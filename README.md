# Arduino AVR Programming with VSCode and PlatformIO in C/C++ 

## Installation

1. Install [VS Code](https://platformio.org/platformio-ide)
2. Install PlatformIO IDE Extension in VS Code
3. Install Arduino Extension in VS Code

## Install GIT on your computer

```
git clone https://gitlab.com/meft-ioelec/arduino
```
cd arduino

## Open and compile examples

