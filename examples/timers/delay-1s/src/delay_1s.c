/* *******************************************************
 * File:   interrupt_1hz.c                               *
 * Author: Ines Rainho (90396) & Joana Bugalho (90397)   *
 * Descricao: Geracao de uma interrupcao de frequencia   *
 * 1 Hz recorrendo a um timer.                        	 *               
 * Created on May 2, 2020, 4:05 PM                 		 *
 *********************************************************/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define BAUD 9600  //9600 bits per second

#define UBRRVAL F_CPU/8/BAUD-1 //define baud_prescaler


#define RXB_MASK 0x1f

uint8_t ind;
volatile uint8_t interrupt_flag = 0;

static int uputc(char,FILE*);
void uputs(const char *);

static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	// wait until we can send a new byte 
	UDR0 = (uint8_t) c;
	return 0;
}

/* timer1 interrupt */
ISR(TIMER1_COMPA_vect)
{
	PORTB ^= _BV(PB5);			  // toggle led on pin 13 (PORTB5) 
	ind++;
	interrupt_flag = 1;
}

int main(void)
{
	stdout=&mystdout;

	/* pin config */
    // Define directions for port pins (B) 
	DDRB = (1 << DDB1);  //OC1A Pin as output (digital 9)
	DDRB = (1 << DDB5);  //Led configured as an output

	/* uart config */
	UCSR0A = (1 << U2X0); // Asynchronous double speed mode
	//enable receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	UCSR0C = (3 << UCSZ00); //1 stop bit, 8-bit data

	/* baudrate setings (variable set by macros) */
	//Load upper 8-bits of the BR val into the high byte of UBRR
	UBRR0H = (UBRRVAL) >> 8; 
	// Load lower 8-bits of the BR val into the low byte of UBRR  
	UBRR0L = UBRRVAL;  

	/* timer cfg - CTC mode - 1Hz */
	cli(); // stop interrupts
	TCCR1A = 0; // normal port operation, OC1A disconnected
	TCCR1B = 0; // normal port operation, OC1B disconnected
	TCNT1  = 0; // initialize counter value to 0
	// set compare match register for 1 Hz increments
	OCR1A = 62499; // = 16000000 / (256 * 1) - 1 (must be <65536)
	TCCR1B |= (1 << WGM12); // CTC mode
	TCCR1B |= (1 << CS12) | (0 << CS11) | (0 << CS10); // prescaler 256
	TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt

	/* ADC cfg */
	sei();							  // enable interrupts 
	puts("Inicio\n");

	while (1) {		
		
		if(interrupt_flag){		// se acontecer uma interrupção...
			interrupt_flag = 0; //... fazer reset à flag...
			puts("Ola\n");	//...e imprimir mensagem
		}

	}

	return 0;

}
