/* *****************************************************
 * File:   PWM_variable_f_dc.c                         *
 * Author: Ines Rainho (90396) & Joana Bugalho (90397) *
 * Descricao: Geracao de um sinal PWM de frequencia e  *
 * duty cycle variaveis                                *       
 * Created on May 2, 2020, 4:05 PM                     *
 **************************************************** **/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BAUD 9600  //9600 bits per second

#define UBRRVAL F_CPU/8/BAUD-1 //define baud_prescaler

#define RX_BUFFER_SIZE 32
#define RXB_MASK 0x1f

char rx_buffer[RX_BUFFER_SIZE];
char frequency_str[RX_BUFFER_SIZE];
char duty_cycle_str[RX_BUFFER_SIZE];
long int frequency, duty_cycle;
long int TOP = 62400, COMP = 10000;
unsigned int j;

uint8_t rx_pointer;
volatile uint8_t interrupt_flag = 0;
volatile uint8_t end_flag = 0;
volatile uint8_t flag_inv = 0, flag_ok = 0;


static int uputc(char,FILE*);
void up_8bits(uint8_t num);
void clear_buffer();

static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

// Apaga o que estiver no buffer
void clear_buffer()		
{
	static uint8_t i;
	for (i = 0; i < RX_BUFFER_SIZE; i++)
		rx_buffer[i] = 0;
	rx_pointer = 0;
}

//Função para imprimir caracteres
static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	// Wait until we can send a new byte 
	UDR0 = (uint8_t) c;
	return 0;
}

//Timer1 interrupt
ISR(TIMER1_COMPA_vect)
{
	PORTB ^= _BV(PB5);			// Toggle led on pin 13 (PORTB5) 
	interrupt_flag = 1;
}

// Timer 1 interrupt B associado ao registo OCR1B
ISR(TIMER1_COMPB_vect)
{
	OCR1B = COMP;			// Atualiza o duty cycle
}

//uart receive isr
ISR(USART_RX_vect)
{
	uint8_t c = UDR0;  // Escreve o input na variável c

	rx_buffer[rx_pointer++] = (char) c;	 // Guarda input no rx_buffer

	if (c == '!')		// Se receber o 'end token'
		end_flag = 1;
}

int main(void)
{
	stdout=&mystdout;

	/* pin config */
    // Define directions for port pins (B) 
	DDRB = (1 << DDB1);  //OC1A Pin as output (digital 9)
	DDRB = (1 << DDB5);  //PB5 led configured as an output

	/* uart config */
	UCSR0A = (1 << U2X0);  // Asynchronous double speed mode
	//enables receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	UCSR0C = (3 << UCSZ00);   //1 stop bit, 8-bit data

	/* baudrate settings (variable set by macros) */
	//Load upper 8-bits of the BR val into the high byte of UBRR
	UBRR0H = (UBRRVAL) >> 8;
	// Load lower 8-bits of the BR val into the low byte of UBRR  
	UBRR0L = UBRRVAL;  

	/* timer 1 cfg */
	TCCR1A |= (3 << WGM10) | (1 << COM1B1); // Fast PWM mode 15
	TCCR1B |= (3 << WGM12) | (2 << CS11); // prescaler 256
	TCNT1 = 0;	// initialize counter value to 0
	OCR1A = TOP; // TOP value (frequency)
	OCR1B = COMP; // Compare value (duty cycle)
	TIMSK1 |= (1 << OCIE1B); //enable timer compare interrupt B

	/* ADC cfg */
	sei();				 // enable interrupts
	puts("Inicio\n");

	clear_buffer();

	while (1) {		
		if(end_flag){
			for(int i = 0; i<RX_BUFFER_SIZE; i++){		// Percorre o buffer

				if(rx_buffer[i] == 'F'){		// Alterar a frequência (comando 'F')
					j = 0;

					for(int k = i+1; k<RX_BUFFER_SIZE; k++){  // Percorre o buffer a partir...
						if(rx_buffer[k] == '!'){			  //... do comando 'F' e até '!'
							break;
						}
						if(48 > rx_buffer[k] || rx_buffer[k] > 57){
							puts("Frequência inválida!\n");
							flag_inv = 1;                         //assinala algo de invalido
							break;
						}
						frequency_str[j] = rx_buffer[k];	  // Guarda os caracteres do buffer
						j++;	
					}

					if(flag_inv){                                // se algo estiver invalido
						flag_inv = 0;
						puts("Não foi inserido nenhum comando válido.\n");
						break;
					}

					frequency = atoi(frequency_str);		  // Converte para int
					OCR1A = frequency * 256/16000000 - 1;	  // Atualiza a frequência
					puts("Changed Frequency!\n");	
					flag_ok = 1;                              
				}

				if(rx_buffer[i] == 'D'){			// Alterar o duty cycle (comando 'D')
					j = 0;

					for(int k = i+1; k<RX_BUFFER_SIZE; k++){  // Percorre o buffer a partir...
						if(rx_buffer[k] == '!'){			  //... do comando 'D' e até '!'
							break;
						}
						if(48 > rx_buffer[k] || rx_buffer[k] > 57){
							puts("Duty cycle inválido!\n");
							flag_inv = 1;                          //assinala algo de invalido
							break;
						}
						duty_cycle_str[j] = rx_buffer[k];	  // Guarda os caracteres do buffer
						j++;	
					}
					if(flag_inv){                                // se algo estiver invalido
						flag_inv = 0;
						puts("Não foi inserido nenhum comando válido.\n");
						break;
					}

					duty_cycle = atoi(duty_cycle_str);		  // Converte para int
					COMP = duty_cycle * OCR1A/100;			  // Atualiza o duty cycle
					puts("Changed Duty Cycle!\n");
					flag_ok = 1;
				}
				else{
					if(!flag_ok){
						puts("Não foi inserido nenhum comando válido.\n");
						break;
					}
				}
				
			}
            end_flag= 0;
			flag_ok=0;
			clear_buffer();		// Limpa o buffer
		}
	}
	return 0;
}
