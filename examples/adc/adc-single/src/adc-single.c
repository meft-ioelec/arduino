/* *****************************************************
 * File:   ADC_potentiometer.c                         *
 * Author: Ines Rainho (90396) & Joana Bugalho (90397) *
 * Descricao: Conversão ADC de um sinal DC proveniente *
 * de um potenciometro ligado a 5V e GND               *       
 * Created on June 4, 2020, 4:05 PM                     *
 **************************************************** **/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Arduino.h>

#define BAUD 9600  //9600 bits per second

#define UBRRVAL F_CPU/8/BAUD-1 //define baud_prescaler

#define RX_BUFFER_SIZE 32
#define RXB_MASK 0x1f

char rx_buffer[RX_BUFFER_SIZE];

uint8_t rx_pointer;
volatile uint8_t interrupt_flag = 0;
volatile uint8_t end_flag = 0;
volatile uint8_t flag_inv = 0, flag_ok = 0;

static int uputc(char,FILE*);
void up_8bits(uint8_t num);
void clear_buffer();

static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

// Apaga o que estiver no buffer
void clear_buffer()		
{
	static uint8_t i;
	for (i = 0; i < RX_BUFFER_SIZE; i++)
		rx_buffer[i] = 0;
	rx_pointer = 0;
}

//Função para imprimir caracteres
static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	// Wait until we can send a new byte 
	UDR0 = (uint8_t) c;
	return 0;
}

//uart receive isr
ISR(USART_RX_vect)
{
	uint8_t c = UDR0;  // Escreve o input na variável c

	rx_buffer[rx_pointer++] = (char) c;	 // Guarda input no rx_buffer

	if (c == '!')		// Se receber o 'end token'
		end_flag = 1;
}



int main(void)
{
	stdout=&mystdout;

	/* uart config */
	UCSR0A = (1 << U2X0);  // Asynchronous double speed mode
	//enables receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	UCSR0C = (3 << UCSZ00);   //1 stop bit, 8-bit data

	/* baudrate settings (variable set by macros) */
	//Load upper 8-bits of the BR val into the high byte of UBRR
	UBRR0H = (UBRRVAL) >> 8;
	// Load lower 8-bits of the BR val into the low byte of UBRR  
	UBRR0L = UBRRVAL;  

	/* ADC cfg */
	ADMUX = (0 << MUX0)  | (1 << REFS0); // channel adc0, AVcc with external capacitor at AREF pin
	ADCSRA = (1 << ADEN) | (7 << ADPS0); // enable adc, prescaler 128
	ADMUX |= (1 << ADLAR);  //left adjust

	sei();				 // enable interrupts

	printf("Inicio\n");

	clear_buffer();

	while (1) {		
		if(end_flag){
			for(int i = 0; i<RX_BUFFER_SIZE; i++){		// Percorre o buffer

				if(rx_buffer[i] == 'S'){               // Se ler o 'S', faz a conversão
					ADCSRA |= (1 << ADSC);             // inicia a conversão
					while (!(ADCSRA & (1 << ADIF)));   // espera que a conversão termine
					ADCSRA |= (1<<ADIF);               // faz set à flag de interrupção do adc
					printf("%d\n", ADC);
					if(rx_buffer[i+1] != '!')          // verifica se o comando é 'S!'
						flag_ok = 0;
					else
						flag_ok = 1;
					
				}            
				else{
					if(!flag_ok){
						puts("Não foi inserido nenhum comando válido.\n");
						break;
					}
				}
			}
			
			end_flag = 0;
			flag_ok = 0;
			clear_buffer();		// Limpa o buffer
		}
	}
	return 0;
}
