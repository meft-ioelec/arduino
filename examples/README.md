# Arduino Codes for the Atmega328p board
## Open in VSCode and compile

###  hello-blink
First  example. Blinking board. Copied for the Arduino IDE

### Timers
More advanced codes using low level programming of the TIMER peripherals

### ADC
More advanced codes using low level programming of the ADC peripheral
