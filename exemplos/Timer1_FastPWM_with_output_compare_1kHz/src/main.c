#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UBRRVAL F_CPU/8/9600-1 /* 9600 can be chanded into other values to get different baud rates*/

#define RXB_SZ 32
#define RXB_MASK 0x1f


static int uputc(char,FILE*);

/* Defines how to "print" one char on the serial terminal*/
static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

/* Uses the uputc to put out strings with puts function*/
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

int main(void)
{
	stdout=&mystdout;

	/* pin config */
    // Define directions for port pins (B) 
	DDRB |= (1 << DDB1);  //OC1A Pin as output (digital 9)
	DDRB |= (1 << DDB5);  //Led configured as an output

	/* uart config */
	UCSR0A = (1 << U2X0); // Asynchronous double speed mode
	//enable receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	UCSR0C = (3 << UCSZ00); //1 stop bit, 8-bit data

	/* baudrate setings (variable set by macros) */
	//Load upper 8-bits of the BR val into the high byte of UBRR
	UBRR0H = (UBRRVAL) >> 8; 
	// Load lower 8-bits of the BR val into the low byte of UBRR  
	UBRR0L = UBRRVAL;  

	/* timer cfg - Fast PWM mode - 1Hz */
	cli(); // stop interrupts
	TCCR1A = 0; // normal port operation, OC1A disconnected
	TCCR1B = 0; // normal port operation, OC1B disconnected
	TCNT1  = 0; // initialize counter value to 0

	TCCR1A |= (1 << COM1A1)| (0 << COM1A0) ;	/* Clear OC1A on compare match (set output to low level). p108 */ 
	TCCR1A |= (1 << WGM11) | (0 << WGM10); /**/
	
	TCCR1B |= (1 << WGM13) | (1 << WGM12); // p109
	TCCR1B |= (0 << CS12) | (0 << CS11) | (1 << CS10); // no prescaler, clock select p110

	ICR1 = 15999; /* 1khz frequency */
	OCR1A = 7999; /* 50% duty */

	TIMSK1 = (0 << ICIE1);

	sei();							  // enable interrupts 
	puts("PWM na porta D9\n");

}
