#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UBRRVAL F_CPU/8/9600-1 /* 9600 can be chanded into other values to get different baud rates*/

#define RXB_SZ 32
#define RXB_MASK 0x1f

int period_counter = 0;


/* timer1 interrupt */
ISR(TIMER0_COMPA_vect)
{
	period_counter++;
}


static int uputc(char,FILE*);

/* Defines how to "print" one char on the serial terminal*/
static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

/* Uses the uputc to put out strings with puts function*/
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

int main(void)
{
	stdout=&mystdout;

	/* pin config */
    // Define directions for port pins (B) 
	DDRD |= (1 << DDD6);  //OC0A Pin as output (digital6)
	DDRB |= (1 << DDB5);  //Led configured as an output

	/* uart config */
	UCSR0A = (1 << U2X0); // Asynchronous double speed mode
	//enable receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	UCSR0C = (3 << UCSZ00); //1 stop bit, 8-bit data

	/* baudrate setings (variable set by macros) */
	//Load upper 8-bits of the BR val into the high byte of UBRR
	UBRR0H = (UBRRVAL) >> 8; 
	// Load lower 8-bits of the BR val into the low byte of UBRR  
	UBRR0L = UBRRVAL;  

	/* timer cfg - CTC mode - 40kHz */
	cli(); // stop interrupts
	TCCR0A = 0; // normal port operation, OC1A disconnected
	TCCR0B = 0; // normal port operation, OC1B disconnected
	TCNT0  = 0; // initialize counter value to 0

	TCCR0A |= (0 << COM0A1)| (1 << COM0A0) ;	/* Clear OC0A on compare match (set output to low level). p85 */ 
	TCCR0A |= (1 << WGM01) | (0 << WGM00); /*p86*/
	
	TCCR0B |= (0 << WGM02); // p86
	TCCR0B |= (0 << CS02) | (0 << CS01) | (1 << CS00); // no prescaler, clock select p110

	OCR0A = 199; /* 50% duty */

	TIMSK0 |= (1 << OCIE0A); // enable timer compare interrut

	sei();							  // enable interrupts 
	puts("PWM na porta D6\n");

	while (1) {		
		if (period_counter % 100){
			puts("here!");
		}
	}


}
